const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
module.exports = (passport) => {
    passport.serializeUser((user, done) => {
        done(null, user);
    });
    passport.deserializeUser((user, done) => {
        done(null, user);
    });
    passport.use(new GoogleStrategy({
            clientID: '59609860256-rj1vb4vvdhn06e6j1q6hsh4a1r3boafi.apps.googleusercontent.com',
            clientSecret: '9Kz4cqlI8YwI-yr8pU2VNZhq',
            callbackURL: 'http://localhost:3002/auth/google/callback'
        },
        (token, refreshToken, profile, done) => {
            return done(null, {
                profile: profile,
                token: token
            });
        }));
};