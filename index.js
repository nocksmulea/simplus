const express = require('express'),
app = express(),
passport = require('passport'),
auth = require('./auth'),
cookieParser = require('cookie-parser'),
cookieSession = require('cookie-session');

auth(passport);
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieSession({
    name: 'session',
    keys: ['Nocks'],
    maxAge: 24 * 60 * 60 * 2 // valid for 2 days
}));
app.use(cookieParser());

const path = require('path')
const PORT = process.env.PORT || 3002

app
    .set('views', path.join(__dirname, 'views'))
    .set('view engine', 'ejs')
    .get('/', (req, res) => {

        if (req.session.token) {
            res.cookie('token', req.session.token);
            let email_domain = req.session.email.split('@');
            email_domain = email_domain[1];

            if(email_domain=='simplusinnovation.com'){
                res.render('pages/content',{username:req.session.username});
            }
            else{

                res.render('pages/error');   
            }


        } else {
            res.cookie('token', '')
            res.render('pages/index')
        }
    })
    .get('/auth/google', passport.authenticate('google', {
        scope: ['https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile']
    }))
    .get('/auth/google/callback',
        passport.authenticate('google', {
            failureRedirect: '/'
        }),
        (req, res) => {

            req.session.token = req.user.token;
            req.session.email =JSON.parse(req.user.profile._raw).email;
            req.session.username =req.user.profile.displayName;
            res.redirect('/');
        }
    )
    .get('/logout', (req, res) => {
        req.logout();
        req.session = null;
        res.redirect('/');
    })
    .listen(PORT, () => console.log(`Listening on ${ PORT }`))